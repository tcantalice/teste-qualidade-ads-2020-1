package br.ucsal.bes20201.testequalidade.caderneta20201.business;

public class CalculoBO {

	private static final Double PESO_NOTA1 = 4d;
	private static final Double PESO_NOTA2 = 6d;
	private static final Double SOMA_PESOS = PESO_NOTA1 + PESO_NOTA2;

	// FIXME Este código tem um problema de implementação do requisito. Ele será
	// utilizado para ilustrar testes que revelarão o problema.
	public ConceitoEnum definirConceito(Double media) throws MediaForaFaixaException {
		ConceitoEnum conceito;
		if (media < 3) {
			conceito = ConceitoEnum.REPROVADO;
		} else if (media <= 6) {
			conceito = ConceitoEnum.PROVA_FINAL;
		} else if (media < 10) {
			conceito = ConceitoEnum.APROVADO;
		} else {
			throw new MediaForaFaixaException(media);
		}
		return conceito;
	}

	//FIXME Este código tem um problema de implementação do requisito. Ele será
	// utilizado para ilustrar testes que revelarão o problema.
	public Double calcularMediaPonderada(Double nota1, Double nota2) {
		Double total = nota2 * PESO_NOTA1 + nota1 * PESO_NOTA2;
		Double media = total / SOMA_PESOS;
		return media;
	}

}
