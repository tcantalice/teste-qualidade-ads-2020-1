package br.ucsal.testequalidade20162.builder;

import java.time.LocalDateTime;

import br.ucsal.testequalidade20162.domain.Multa;
import br.ucsal.testequalidade20162.domain.TipoMultaEnum;

public class MultaObjectMother {

	public static Multa umaMultaLeve() {
		Multa multa = new Multa();
		multa.setDataHora(LocalDateTime.of(2020, 10, 5, 12, 5));
		multa.setLocal("Pituba");
		multa.setTipo(TipoMultaEnum.LEVE);
		return multa;
	}

	public static Multa umaMultaGrave() {
		Multa multa = new Multa();
		multa.setDataHora(LocalDateTime.of(2020, 10, 5, 12, 5));
		multa.setLocal("Pituba");
		multa.setTipo(TipoMultaEnum.GRAVE);
		return multa;
	}

}
