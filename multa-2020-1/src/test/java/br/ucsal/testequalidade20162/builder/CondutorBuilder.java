package br.ucsal.testequalidade20162.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.ucsal.testequalidade20162.domain.Condutor;
import br.ucsal.testequalidade20162.domain.Multa;

public class CondutorBuilder {

	private static final Integer NUMERO_CNH_DEFAULT = 1;
	private static final String NOME_DEFAULT = "Claudio";
	private static final List<Multa> MULTAS_DEFAULT = new ArrayList<>();

	private Integer numeroCnh = NUMERO_CNH_DEFAULT;
	private String nome = NOME_DEFAULT;
	private List<Multa> multas = MULTAS_DEFAULT;

	private CondutorBuilder(){
		
	}
	
	public static CondutorBuilder umCondutor() {
		return new CondutorBuilder();
	}
	
	public CondutorBuilder comMultas(Multa...multas) {
		this.multas.addAll(Arrays.asList(multas));
		return this;
	}

	public Condutor build() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
